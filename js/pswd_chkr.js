//all functions follow guide of password check example from canvas


//Required Field; Max 50 Char, Min 6 Char
function usernameVer (){
    let username = document.getElementById("username");
    if (username) {

        //username Length test and display errors
        if (username.value.length < 6 || username.value.length > 50) {
            let userDiv = document.getElementById("userNameDiv");
            if (userDiv){
                userDiv.classList.remove("has-success");
                userDiv.classList.add("has-error");
            }
            let userErr = document.getElementById("usernameErr");
            if (userErr){
                userErr.classList.remove("hide");
                userErr.classList.add("show");
            }
            return false;
            <!-- length is valid. return true and adjust display -->
        } else {
            let userDiv = document.getElementById("userNameDiv");
            if (userDiv){
                userDiv.classList.remove("has-error");
                userDiv.classList.add("has-success");
            }
            let userErr = document.getElementById("usernameErr");
            if (userErr){
                userErr.classList.remove("show");
                userErr.classList.add("hide");
            }
            return true;
        }
    }
}

//required; min 6, max 60 ; need 1 1 Capital, 1 Lower, 1 Digit, 1 Special (4 separate tests)
function passwordVer() {
    let passOne = document.getElementById("password"),
        passTwo = document.getElementById("password1"),
        test1 = passOne.value.search(/\S*[A-Z]+\S*/),
        test2 = passOne.value.search(/\S*[a-z]+\S*/),
        test3 = passOne.value.search(/\S*[0-9]+\S*/),
        test4 = passOne.value.search(/\S*\W+\S*/),
        fullTest = test1 + test2 + test3 + test4,
        isValid = false;


    //If password has content, test to see it is valid.
    if (passOne.value !== "") {
        if (passOne.value.length < 8 || passOne.value.length >= 50 || fullTest < 0) {
            let passOneDiv = document.getElementById("passwordDiv");
            if (passOneDiv) {
                passOneDiv.classList.remove("has-success");
                passOneDiv.classList.add("has-error");
            }
            let passOneErr = document.getElementById("passErr");
            if (passOneErr) {
                passOneErr.classList.remove("hide");
                passOneErr.classList.add("show");
            }
        } //input valid. return true and adjust display -->
        else {
            isValid = true;
            let passOneDiv = document.getElementById("passwordDiv");
            if (passOneDiv) {
                passOneDiv.classList.remove("has-error");
                passOneDiv.classList.add("has-success");
            }

            let passOneErr = document.getElementById("passErr");
            if (passOneErr) {
                passOneErr.classList.remove("show");
                passOneErr.classList.add("hide");
            }
        }
    }
    //compare passwords to check if they match
    if (passOne && passTwo) {
        // passwords don't match and passTwo has content -> Red outline passOne and Two, Error msg passTwo
        if ((passOne.value !== passTwo.value) && (passTwo.value !== "")) {
            let passOneDiv = document.getElementById("passwordDiv"),
                passTwoDiv = document.getElementById("password1Div");
            if (passOneDiv && passTwoDiv) {
                passOneDiv.classList.remove("has-success");
                passOneDiv.classList.add("has-error");
                passTwoDiv.classList.remove("has-success");
                passTwoDiv.classList.add("has-error");
            }

            let passTwoErr = document.getElementById("password1Err")
            if (passTwoErr) {
                passTwoErr.classList.remove("hide");
                passTwoErr.classList.add("show");
            }
        }
    }
    //If passOne and Two are equal as well as valid, Success!
    if (passOne.value === passTwo.value) {
        if (isValid) {
            let passOneDiv = document.getElementById("passwordDiv"),
                passTwoDiv = document.getElementById("password1Div");
            if (passOneDiv && passTwoDiv) {
                passOneDiv.classList.remove("has-error");
                passOneDiv.classList.add("has-success");
                passTwoDiv.classList.remove("has-error");
                passTwoDiv.classList.add("has-success");
            }
            let passTwoErr = document.getElementById("password1Err")
            if (passTwoErr) {
                passTwoErr.classList.remove("show");
                passTwoErr.classList.add("hide");
            }
            return true;

            //passOne and Two are equal but not valid, assign has error to both, display passErr only
        } else {
            let passOneDiv = document.getElementById("passwordDiv"),
                passTwoDiv = document.getElementById("password1Div");
            if (passOneDiv && passTwoDiv) {
                passOneDiv.classList.remove("has-success");
                passOneDiv.classList.add("has-error");
                passTwoDiv.classList.remove("has-success");
                passTwoDiv.classList.add("has-error");
            }
            let passOneErr = document.getElementById("passErr"),
                passTwoErr = document.getElementById("password1Err");
            if (passOneErr && passTwoErr) {
                passOneErr.classList.remove("hide");
                passOneErr.classList.add("show");
                passTwoErr.classList.remove("show");
                passTwoErr.classList.add("hide");
            }
        }
        return false
    }
}

//required; format is x@x.x
function emailVer()
{
    let email = document.getElementById("email");
    if (email){
        email.value = email.value.replace(/\s/,"");
        let index = email.value.search(
            /^\S+\@\S+\.\S+$/
        );

        //testing format x@x.x format
        if (email.value === "" || index < 0){
            let emailDiv = document.getElementById("emailDiv");
            if (emailDiv){
                emailDiv.classList.remove("has-success");
                emailDiv.classList.add("has-error");
            }
            let emailErr = document.getElementById("emailErr");
            if (emailErr){
                emailErr.classList.remove("hide");
                emailErr.classList.add("show");
            }
            //valid. return true and display correct errors
        } else {
            let emailDiv = document.getElementById("emailDiv");
            if (emailDiv){
                emailDiv.classList.remove("has-error");
                emailDiv.classList.add("has-success");
            }

            let emailErr = document.getElementById("emailErr");
            if (emailErr){
                emailErr.classList.remove("show");
                emailErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//required; min 10, max 12; Script to convert to 123-456-7890 format
function phoneVer(){
    let phoneNumber = document.getElementById("phone");
    if (phoneNumber){
        phoneNumber.value = phoneNumber.value.replace(/\D/g,"");
        // Fail if !10 and display errors
        if (phoneNumber.value.length !== 10){
            let phoneNumberDiv = document.getElementById("phoneDiv");
            if (phoneNumberDiv){
                phoneNumberDiv.classList.remove("has-success");
                phoneNumberDiv.classList.add("has-error");
            }
            let phoneNumberErr = document.getElementById("phoneErr");
            if (phoneNumberErr){
                phoneNumberErr.classList.remove("hide");
                phoneNumberErr.classList.add("show");
            }

            //length is valid; return true and change display
        } else {

            phoneNumber.value = phoneNumber.value.slice(0,3) + "-" + phoneNumber.value.slice(3,6) +
                "-" + phoneNumber.value.slice(6,10);
            let phoneNumberDiv = document.getElementById("phoneDiv");
            if (phoneNumberDiv){
                phoneNumberDiv.classList.remove("has-error");
                phoneNumberDiv.classList.add("has-success");
            }

            let phoneNumberErr = document.getElementById("phoneErr");
            if (phoneNumberErr){
                phoneNumberErr.classList.remove("show");
                phoneNumberErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//required
function genderVer(){
    let male = document.getElementById("maleGender"),
        female = document.getElementById("femaleGender"),
        other = document.getElementById("otherGender");
    if (male && female && other){
        //Fail if no choices are checked and display error
        if (!male.checked && !female.checked && !other.checked){
            let genderDiv = document.getElementById("genderDiv");
            if (genderDiv){
                genderDiv.classList.remove("has-success");
                genderDiv.classList.add("has-error");
            }
            let genderErr = document.getElementById("genderErr");
            if (genderErr){
                genderErr.classList.remove("hide");
                genderErr.classList.add("show");
            }

            //choice made. return true and display
        } else {
            let genderDiv = document.getElementById("genderDiv");
            if (genderDiv){
                genderDiv.classList.remove("has-error");
                genderDiv.classList.add("has-success");
            }

            let genderErr = document.getElementById("genderErr");
            if (genderErr){
                genderErr.classList.remove("show");
                genderErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//required
function maritalVer(){
    let single = document.getElementById("single"),
        married = document.getElementById("married"),
        seperated = document.getElementById("seperated"),
        divorced = document.getElementById("divorced"),
        widowed = document.getElementById("widowed");

    if (single && married && seperated && divorced && widowed){
        //Fail no choices made and display error
        if (!single.checked && !married.checked && !seperated.checked && !divorced.checked && !widowed.checked){
            let maritalDiv = document.getElementById("maritalDiv");
            if (maritalDiv){
                maritalDiv.classList.remove("has-success");
                maritalDiv.classList.add("has-error");
            }
            let martialErr = document.getElementById("martialErr");
            if (martialErr){
                martialErr.classList.remove("hide");
                martialErr.classList.add("show");
            }

            //choice made. return true and display
        } else {
            let maritalDiv = document.getElementById("maritalDiv");
            if (maritalDiv){
                maritalDiv.classList.remove("has-error");
                maritalDiv.classList.add("has-success");
            }
            let martialErr = document.getElementById("martialErr");
            if (martialErr){
                martialErr.classList.remove("show");
                martialErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//max input 50; display corresponding errors
function firstnameVer() {
    let nameIn = document.getElementById("firstName");
    if (nameIn) {

        ///First Name Length; max 50 and display errors
        if (nameIn.value.length < 1 || nameIn.value.length > 50) {
            let nameInDiv = document.getElementById("firstNameDiv");
            if (nameInDiv) {
                nameInDiv.classList.remove("has-success");
                nameInDiv.classList.add("has-error");
            }
            let nameInErr = document.getElementById("firstNameErr");
            if (nameInErr) {
                nameInErr.classList.remove("hide");
                nameInErr.classList.add("show");
            }
        } else {   //length is valid. return true and adjust display
            let nameInDiv = document.getElementById("firstNameDiv");
            if (nameInDiv) {
                nameInDiv.classList.remove("has-error");
                nameInDiv.classList.add("has-success");
            }
            let nameInErr = document.getElementById("firstNameErr");
            if (nameInErr) {
                nameInErr.classList.remove("show");
                nameInErr.classList.add("hide");
            }
            return true;
        }
    }
    return false
}

//max 50 display errors
    function lastnameVer() {
        let nameLast = document.getElementById("lastName");
        if (nameLast) {
            //Last Name Length test; max 50 and display errors
            if (nameLast.value.length < 1 || nameLast.value.length > 50) {
                let nameLastDiv = document.getElementById("lastNameDiv");
                if (nameLastDiv) {
                    nameLastDiv.classList.remove("has-success");
                    nameLastDiv.classList.add("has-error");
                }
                let nameLastErr = document.getElementById("lastNameErr");
                if (nameLastErr) {
                    nameLastErr.classList.remove("hide");
                    nameLastErr.classList.add("show");
                }

            } else {   //length is valid. return true and adjust display
                let nameLastDiv = document.getElementById("lastNameDiv");
                if (nameLastDiv) {
                    nameLastDiv.classList.remove("has-error");
                    nameLastDiv.classList.add("has-success");
                }

                let nameLastErr = document.getElementById("lastNameErr");
                if (nameLastErr) {
                    nameLastErr.classList.remove("show");
                    nameLastErr.classList.add("hide");
                }
                return true;
            }
        }
        return false;
    }

//Address 1 Length max 100 and display errors
    function addressOneVer() {
        let street = document.getElementById("address1");
        if (street) {

            // Address 1 Length max 100 and display errors
            if (street.value.length < 1 || street.value.length > 100) {
                let streetDiv = document.getElementById("address1Div");
                if (streetDiv) {
                    streetDiv.classList.remove("has-success");
                    streetDiv.classList.add("has-error");
                }
                let streetErr = document.getElementById("address1Err");
                if (streetErr) {
                    streetErr.classList.remove("hide");
                    streetErr.classList.add("show");
                }

                //length is valid. return true and display errors
            } else {
                let streetDiv = document.getElementById("address1Div");
                if (streetDiv) {
                    streetDiv.classList.remove("has-error");
                    streetDiv.classList.add("has-success");
                }

                let streetErr = document.getElementById("address1Err");
                if (streetErr) {
                    streetErr.classList.remove("show");
                    streetErr.classList.add("hide");
                }
                return true;
            }
        }
        return false;
    }

    //max 100 and display errors OPTIONAL
    function addressTwoVer() {
        let aptNumber = document.getElementById("address2");
        if (aptNumber) {

            //max 100 and display errors
            if (aptNumber.value.length > 100) {
                let aptNumberDiv = document.getElementById("address2Div");
                if (aptNumberDiv) {
                    aptNumberDiv.classList.remove("has-success");
                    aptNumberDiv.classList.add("has-error");
                }
                let aptNumberErr = document.getElementById("address2Err");
                if (aptNumberErr) {
                    aptNumberErr.classList.remove("hide");
                    aptNumberErr.classList.add("show");
                }

                //length is valid. return true and display accordingly
            } else {
                let aptNumberDiv = document.getElementById("address2Div");
                if (aptNumberDiv) {
                    aptNumberDiv.classList.remove("has-error");
                    aptNumberDiv.classList.add("has-success");
                }

                let aptNumberErr = document.getElementById("address2Err");
                if (aptNumberErr) {
                    aptNumberErr.classList.remove("show");
                    aptNumberErr.classList.add("hide");
                }
                return true;
            }
        }
        return false;
    }

//max 50 display errors
    function cityVer() {
        let city = document.getElementById("city");
        if (city) {

            //max 50 and display errors
            if (city.value.length < 1 || city.value.length > 50) {
                let cityDiv = document.getElementById("cityDiv");
                if (cityDiv) {
                    cityDiv.classList.remove("has-success");
                    cityDiv.classList.add("has-error");
                }
                let cityErr = document.getElementById("cityErr");
                if (cityErr) {
                    cityErr.classList.remove("hide");
                    cityErr.classList.add("show");
                }

                //valid. return true and adjust display
            } else {
                let cityDiv = document.getElementById("cityDiv");
                if (cityDiv) {
                    cityDiv.classList.remove("has-error");
                    cityDiv.classList.add("has-success");
                }

                let cityErr = document.getElementById("cityErr");
                if (cityErr) {
                    cityErr.classList.remove("show");
                    cityErr.classList.add("hide");
                }
                return true;
            }
        }
        return false;
    }


//required max 50
function stateVer()
{
    let state = document.getElementById("state");
    if (state){
        /* State Choice Test - Fail if .value == # and display errors */
        if (state.value === ""){
            let stateDiv = document.getElementById("stateDiv");
            if (stateDiv){
                stateDiv.classList.remove("has-success");
                stateDiv.classList.add("has-error");
            }
            let stateErr = document.getElementById("stateErr");
            if (stateErr){
                stateErr.classList.remove("hide");
                stateErr.classList.add("show");
            }

            <!-- Else, selection is valid. return true and adjust display -->
        } else {
            let stateDiv = document.getElementById("stateDiv");
            if (stateDiv){
                stateDiv.classList.remove("has-error");
                stateDiv.classList.add("has-success");
            }

            let stateErr = document.getElementById("stateErr");
            if (stateErr){
                stateErr.classList.remove("show");
                stateErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//required, min 5 max 10; match 12345 or 12345-6789
function zipVer() {
    let zipCode = document.getElementById("zip");
    if (zipCode) {
        zipCode.value = zipCode.value.replace(/\D/g, "");

        /* Length test  5 and 10-1(dash) = 9 and display errors */
        if ((zipCode.value.length !== 5 && zipCode.value.length !== 9) || !zipCode.value.match(/[0-9]+/)) {
            let zipCodeDiv = document.getElementById("zipDiv");
            if (zipCodeDiv) {
                zipCodeDiv.classList.remove("has-success");
                zipCodeDiv.classList.add("has-error");
            }
            let zipCodeErr = document.getElementById("zipErr");
            if (zipCodeErr) {
                zipCodeErr.classList.remove("hide");
                zipCodeErr.classList.add("show");
            }
            //valid. return true and adjust display
        } else {
            if (zipCode.value.length === 9) {
                zipCode.value = zipCode.value.slice(0, 5) + "-" + zipCode.value.slice(5)
            }
            let zipCodeDiv = document.getElementById("zipDiv");
            if (zipCodeDiv) {
                zipCodeDiv.classList.remove("has-error");
                zipCodeDiv.classList.add("has-success");
            }

            let zipCodeErr = document.getElementById("zipErr");
            if (zipCodeErr) {
                zipCodeErr.classList.remove("show");
                zipCodeErr.classList.add("hide");
            }
            return true;
        }
    }
    return false;
}

//required and only numbers to ensure mm/dd/yyyy
function dobVer(){
    let birth = document.getElementById("dob");
    if (birth) {
        //check for numbers
        if (birth.value.match(/[0-9]+/)){
            let dobDiv = document.getElementById("dobDiv");
            if (dobDiv) {
                dobDiv.classList.remove("has-error");
                dobDiv.classList.add("has-success");
            }
            let dateErr = document.getElementById("dateErr");
            if (dateErr) {
                dateErr.classList.remove("show");
                dateErr.classList.add("hide");
            }
            return true;

            //invalid input. Display errors
        } else {
            let dobDiv = document.getElementById("dobDiv");
            if (dobDiv) {
                dobDiv.classList.remove("has-success");
                dobDiv.classList.add("has-error");
            }
            let dateErr = document.getElementById("dateErr");
            if (dateErr) {
                dateErr.classList.remove("hide");
                dateErr.classList.add("show");
            }
            return false;
        }
    }
}



//Check for invalid input all required fields
//Submit if all checks return true
function submitVer() {
    let currentForm = document.getElementById("registrationForm");
    if (currentForm) {
        let user = usernameVer(),
            pass = passwordVer(),
            first = firstnameVer(),
            last = lastnameVer(),
            address = addressOneVer(),
            city = cityVer(),
            state = stateVer(),
            zip = zipVer(),
            phone = phoneVer(),
            email = emailVer(),
            gender = genderVer(),
            marital = maritalVer(),
            dob = dobVer();

        //make everything required that needs to be
        let isCurrentValid = user & pass & first & last & address & city & state & zip & phone & email & gender & marital & dob;
        if (isCurrentValid) {
            resetVer();
            return true;
        }
        return false;
    }
}

//reset error message and values
function resetVer(){
    let userDiv = document.getElementById("userNameDiv");
    let userErr = document.getElementById("usernameErr");
    if (userDiv){
        userDiv.classList.remove("has-error");
        userDiv.classList.remove("has-success");
    }
    if (userErr){
        userErr.classList.remove("show");
        userErr.classList.add("hide");
    }

    let passOneDiv = document.getElementById("passwordDiv"),
        passOneErr = document.getElementById("passErr");
    if (passOneDiv){
        passOneDiv.classList.remove("has-error");
        passOneDiv.classList.remove("has-success");
    }
    if (passOneErr){
        passOneErr.classList.remove("show");
        passOneErr.classList.add("hide");
    }

    let passTwoDiv = document.getElementById("password1Div"),
        passTwoErr = document.getElementById("password1Err");
    if (passTwoDiv){
        passTwoDiv.classList.remove("has-error");
        passTwoDiv.classList.remove("has-success");
    }
    if (passTwoErr){
        passTwoErr.classList.remove("show");
        passTwoErr.classList.add("hide");
    }

    let nameInputDiv = document.getElementById("firstNameDiv"),
        nameInputErr = document.getElementById("firstNameErr");
    if (nameInputDiv){
        nameInputDiv.classList.remove("has-error");
        nameInputDiv.classList.remove("has-success");
    }
    if (nameInputErr){
        nameInputErr.classList.remove("show");
        nameInputErr.classList.add("hide");
    }

    let nameLastDiv = document.getElementById("lastName"),
        nameLastErr = document.getElementById("lastNameErr");
    if (nameLastDiv){
        nameLastDiv.classList.remove("has-error");
        nameLastDiv.classList.remove("has-success");
    }
    if (nameLastErr){
        nameLastErr.classList.remove("show");
        nameLastErr.classList.add("hide");
    }

    let address1Div = document.getElementById("address1Div"),
        address1Err = document.getElementById("address1Err");
    if (address1Div){
        address1Div.classList.remove("has-error");
        address1Div.classList.remove("has-success");
    }
    if (address1Err){
        address1Err.classList.remove("show");
        address1Err.classList.add("hide");
    }

    let cityDiv = document.getElementById("cityDiv"),
        cityErr = document.getElementById("cityErr");
    if (cityDiv){
        cityDiv.classList.remove("has-error");
        cityDiv.classList.remove("has-success");
    }
    if (cityErr){
        cityErr.classList.remove("show");
        cityErr.classList.add("hide");
    }

    let stateDiv = document.getElementById("stateDiv"),
        stateErr = document.getElementById("stateErr");
    if (stateDiv){
        stateDiv.classList.remove("has-error");
        stateDiv.classList.remove("has-success");
    }
    if (stateErr){
        stateErr.classList.remove("show");
        stateErr.classList.add("hide");
    }


    let zipDiv = document.getElementById("zipDiv"),
        zipErr = document.getElementById("zipErr");
    if (zipDiv){
        zipDiv.classList.remove("has-error");
        zipDiv.classList.remove("has-success");
    }
    if (zipErr){
        zipErr.classList.remove("show");
        zipErr.classList.add("hide");
    }

    let phoneDiv = document.getElementById("phoneDiv"),
        phoneErr = document.getElementById("phoneErr");
    if (phoneDiv){
        phoneDiv.classList.remove("has-error");
        phoneDiv.classList.remove("has-success");
    }
    if (phoneErr){
        phoneErr.classList.remove("show");
        phoneErr.classList.add("hide");
    }

    let emailDiv = document.getElementById("emailDiv"),
        emailErr = document.getElementById("emailErr");
    if (emailDiv){
        emailDiv.classList.remove("has-error");
        emailDiv.classList.remove("has-success");
    }
    if (emailErr){
        emailErr.classList.remove("show");
        emailErr.classList.add("hide");
    }


    let genderDiv = document.getElementById("genderDiv"),
        genderErr = document.getElementById("genderErr");
    if (genderDiv){
        genderDiv.classList.remove("has-error");
        genderDiv.classList.remove("has-success");
    }
    if (genderErr){
        genderErr.classList.remove("show");
        genderErr.classList.add("hide");
    }

    let maritalDiv = document.getElementById("maritalDiv"),
        martialErr = document.getElementById("martialErr");
    if (maritalDiv){
        maritalDiv.classList.remove("has-error");
        maritalDiv.classList.remove("has-success");
    }
    if (martialErr){
        martialErr.classList.remove("show");
        martialErr.classList.add("hide");
    }

    let dobDiv = document.getElementById("dobDiv"),
        dateErr = document.getElementById("dateErr");
    if (dobDiv){
        dobDiv.classList.remove("has-error");
        dobDiv.classList.remove("has-success");
    }
    if (dateErr){
        dateErr.classList.remove("show");
        dateErr.classList.add("hide");
    }
}

//on blur events for all verify fields
    function registerHandlers() {
        document.getElementById("username").onblur = usernameVer;
        document.getElementById("password").onblur = passwordVer;
        document.getElementById("password1").onblur = passwordVer;
        document.getElementById("email").onblur = emailVer;
        document.getElementById("phone").onblur = phoneVer;
        document.getElementById("genderDiv").onblur = genderVer;
        document.getElementById("maritalDiv").onblur = maritalVer;
        document.getElementById("firstName").onblur = firstnameVer;
        document.getElementById("lastName").onblur = lastnameVer;
        document.getElementById("address1").onblur = addressOneVer;
        document.getElementById("address2").onblur = addressTwoVer;
        document.getElementById("city").onblur = cityVer;
        document.getElementById("state").onblur = stateVer;
        document.getElementById("zip").onblur = zipVer;
        document.getElementById("dob").onblur = dobVer;
        document.getElementById("registrationForm").onsubmit = submitVer;
        document.getElementById("registrationForm").onreset = resetVer;
    }
