<?php
  include 'connectInfo.php';
?>

<!DOCTYPE html>
<html lang="en">

<head style="background-color:#757194">
    <?php define("title", "Confirmation");?>
    <title><?php echo title; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="./js/pswd_chkr.js"></script>

</head>

<body>
<?php
  include 'dataSelect.php';
?>
<!-- bring header and navigation menu-->
<div class="container-fluid text-center" style="background-color:#757194">
    <img src="./img/BBLogoShort.jpeg" alt="Beauto Beauty Logo"  style="width:35%; animation:none">
</div>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- create hamburger drop down menu  -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="home.html">Home</a></li>
                <li class="active"><a href="registration.php">Registration </a></li>
                <li><a href="animations.html">Animations </a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid text-center">
    <div class="row content">
        <!-- menu-->
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidenav">
            <p><a href="home.html">Home</a></p>
            <p><a href="registration.php">Registration</a></p>
            <p><a href="animations.html">Animations</a></p>
        </div>
        <!-- content-->
        <form method="POST" id="registrationForm" novalidate action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-left">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <h3>Account Information</h3>
                            <div id="userNameDiv">
                                <label for="username">Username:</label>
                                <input id="username" name="username" class="form-control" type="text" minlength="6"
                                       maxlength="50" value="<?php echo $username;?>" disabled/>
                            </div>
                            <div id="passwordDiv">
                                <label for="password">Password:</label>
                                <input id="password" name="password" class="form-control" type="text"
                                       minlength="8" maxlength="50" value="<?php echo $password;?>" disabled/>
                            </div>
                            <div id="password1Div">
                                <label for="password1">Repeat Password:</label>
                                <input id="password1" name="password1" class="form-control" type="text"
                                       value="<?php echo $password;?>" minlength="8" maxlength="50" disabled/>
                            </div>
                            <div id="emailDiv">
                                <label for="email">Email:</label>
                                <input id="email" name="email" class="form-control" type="email"
                                       placeholder="email@domain.com" value="<?php echo $email;?>" disabled/>
                            </div>
                            <div id="phoneDiv">
                                <label for="phone">Phone:</label>
                                <input id="phone" name="phone" class="form-control" type="tel"
                                       placeholder="xxx-xxx-xxxx" maxlength="12" value="<?php echo $phone;?>" disabled/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div id="genderDiv" class="form-group">
                                    <h3>Gender</h3>
                                    <input disabled  id="maleGender" name="gender" type="radio" <?php if ($gender=="male"){echo "checked";} ?>
                                           value="male"/> <label for="maleGender">Male</label><br/>
                                    <input disabled  id="femaleGender" name="gender" type="radio" <?php if ($gender=="female"){echo "checked";} ?>
                                           value="female"/> <label for="femaleGender">Female</label><br/>
                                    <input disabled  id="otherGender" name="gender" type="radio" <?php if ($gender=="other"){echo "checked";} ?>
                                           value="other"/> <label for="otherGender">Other</label><br/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div id="maritalDiv" class="form-group">
                                    <h3>Marital Status</h3>
                                    <input disabled id="single" name="status" type="radio" <?php if ($martial=="single"){echo "checked";} ?> value="single">
                                    <label for="single">Single</label><br/>
                                    <input disabled  id="married" name="status" type="radio" <?php if ($martial=="married"){echo "checked";} ?> value="married">
                                    <label for="married">Married</label><br>
                                    <input disabled  id="seperated" name="status" type="radio" <?php if ($martial=="seperated"){echo "checked";} ?> value="seperated">
                                    <label for="seperated">Seperated</label><br>
                                    <input disabled  id="divorced" name="status" type="radio" <?php if ($martial=="divorced"){echo "checked";} ?> value="divorced">
                                    <label for="divorced">Divorced</label><br>
                                    <input disabled  id="widowed" name="status" type="radio" <?php if ($martial=="widowed"){echo "checked";} ?> value ="widowed">
                                    <label for="widowed">Widowed</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <div id="firstNameDiv">
                                <h3>Personal Information</h3>
                                <label for="firstName">First Name:</label>
                                <input id="firstName" name="firstName" class="form-control" type="text"
                                       placeholder="Emily" maxlength="50" value="<?php echo $firstName;?>" disabled/>
                            </div>
                            <div id="lastNameDiv">
                                <label for="lastName">Last Name:</label>
                                <input id="lastName" name="lastName" class="form-control" type="text"
                                       placeholder="Beauto" maxlength="50" value="<?php echo $lastName;?>" disabled/>
                            </div>
                            <div id="address1Div">
                                <label for="address1">Address Line 1</label>
                                <input id="address1" name="address1" class="form-control" type="text" maxlength="100" value="<?php echo $address1;?>" disabled/>
                            </div>
                            <div id="address2Div">
                                <label for="address2">Address Line 2</label>
                                <input id="address2" name="address2" class="form-control" type="text"
                                       placeholder="(Optional)" maxlength="100" value="<?php echo $address2;?>" disabled/>
                            </div>
                            <div id="cityDiv">
                                <label for="city">City</label>
                                <input id="city" name="city" class="form-control" type="text" maxlength="50" value="<?php echo $city;?>" disabled/>
                            </div>
                            <div id="stateDiv">
                                <label for="state">State:</label>
                                <select disabled id="state" name="state" class="form-control" value="">
                                    <option value="" selected="selected" hidden="hidden" disabled="disabled" >--Please Select--</option>
                                    <option value="AL" <?php if ($state=="AL") {echo "selected";} ?> >Alabama</option>
                                    <option value="AK" <?php if ($state=="AK") {echo "selected";} ?> >Alaska</option>
                                    <option value="AZ" <?php if ($state=="AZ") {echo "selected";} ?> >Arizona</option>
                                    <option value="AR" <?php if ($state=="AR") {echo "selected";} ?> >Arkansas</option>
                                    <option value="CA" <?php if ($state=="CA") {echo "selected";} ?> >California</option>
                                    <option value="CO"<?php if ($state=="CO") {echo "selected";} ?> >Colorado</option>
                                    <option value="CT"<?php if ($state=="CT") {echo "selected";} ?> >Connecticut</option>
                                    <option value="DE" <?php if ($state=="DE") {echo "selected";} ?> >Delaware</option>
                                    <option value="FL" <?php if ($state=="FL") {echo "selected";} ?> >Florida</option>
                                    <option value="GA" <?php if ($state=="GA") {echo "selected";} ?> >Georgia</option>
                                    <option value="HI" <?php if ($state=="HI") {echo "selected";} ?> >Hawaii</option>
                                    <option value="ID" <?php if ($state=="ID") {echo "selected";} ?> >Idaho</option>
                                    <option value="IL" <?php if ($state=="IL") {echo "selected";} ?> >Illinois</option>
                                    <option value="IN" <?php if ($state=="IN") {echo "selected";} ?> >Indiana</option>
                                    <option value="IA" <?php if ($state=="IA") {echo "selected";} ?> >Iowa</option>
                                    <option value="KS" <?php if ($state=="KS") {echo "selected";} ?> >Kansas</option>
                                    <option value="KY" <?php if ($state=="KY") {echo "selected";} ?> >Kentucky</option>
                                    <option value="LA" <?php if ($state=="LA") {echo "selected";} ?> >Louisiana</option>
                                    <option value="ME" <?php if ($state=="ME") {echo "selected";} ?> >Maine</option>
                                    <option value="MD" <?php if ($state=="MD") {echo "selected";} ?> >Maryland</option>
                                    <option value="MA" <?php if ($state=="MA") {echo "selected";} ?> >Massachusetts</option>
                                    <option value="MI" <?php if ($state=="MI") {echo "selected";} ?> >Michigan</option>
                                    <option value="MN" <?php if ($state=="MN") {echo "selected";} ?> >Minnesota</option>
                                    <option value="MS" <?php if ($state=="MS") {echo "selected";} ?> >Mississippi</option>
                                    <option value="MO" <?php if ($state=="MO") {echo "selected";} ?> >Missouri</option>
                                    <option value="MT" <?php if ($state=="MT") {echo "selected";} ?> >Montana</option>
                                    <option value="NE" <?php if ($state=="NE") {echo "selected";} ?> >Nebraska</option>
                                    <option value="NV" <?php if ($state=="NV") {echo "selected";} ?> >Nevada</option>
                                    <option value="NH" <?php if ($state=="NH") {echo "selected";} ?> >New Hampshire</option>
                                    <option value="NJ" <?php if ($state=="NJ") {echo "selected";} ?> >New Jersey</option>
                                    <option value="NM" <?php if ($state=="NM") {echo "selected";} ?> >New Mexico</option>
                                    <option value="NY" <?php if ($state=="NY") {echo "selected";} ?>>New York</option>
                                    <option value="NC" <?php if ($state=="NC") {echo "selected";} ?> >North Carolina</option>
                                    <option value="ND" <?php if ($state=="ND") {echo "selected";} ?> >North Dakota</option>
                                    <option value="OH" <?php if ($state=="OH") {echo "selected";} ?> >Ohio	</option>
                                    <option value="OK" <?php if ($state=="OK") {echo "selected";} ?> >Oklahoma</option>
                                    <option value="OR" <?php if ($state=="OR") {echo "selected";} ?> >Oregon</option>
                                    <option value="PA" <?php if ($state=="PA") {echo "selected";} ?> >Pennsylvania</option>
                                    <option value="RI" <?php if ($state=="RI") {echo "selected";} ?> >Rhode Island</option>
                                    <option value="SC" <?php if ($state=="SC") {echo "selected";} ?> >South Carolina</option>
                                    <option value="SD" <?php if ($state=="SD") {echo "selected";} ?> >South Dakota</option>
                                    <option value="TN" <?php if ($state=="TN") {echo "selected";} ?> >Tennessee</option>
                                    <option value="TX" <?php if ($state=="TX") {echo "selected";} ?> >Texas</option>
                                    <option value="UT" <?php if ($state=="UT") {echo "selected";} ?> >Utah</option>
                                    <option value="VT" <?php if ($state=="VT") {echo "selected";} ?> >Vermont</option>
                                    <option value="VA" <?php if ($state=="VA") {echo "selected";} ?> >Virginia</option>
                                    <option value="WA" <?php if ($state=="WA") {echo "selected";} ?> >Washington</option>
                                    <option value="WV" <?php if ($state=="WV") {echo "selected";} ?> >West Virginia</option>
                                    <option value="WI" <?php if ($state=="WI") {echo "selected";} ?> >Wisconsin</option>
                                    <option value="WY" <?php if ($state=="WY") {echo "selected";} ?> >Wyoming</option>
                                </select>
                            </div>
                            <div id="zipDiv">
                                <label for="zip">Zip:</label>
                                <input id="zip" name="zip" class="form-control" type="text"
                                       placeholder="12345 or 12345-6789" minlength="5" maxlength="10" value="<?php echo $zip;?>" disabled/>
                            </div>
                            <div id="dobDiv">
                                <label for="dob">Date of Birth</label><br>
                                <input disabled type="date" id="dob" name="dob" maxlength="4" class="form-control" value="<?php echo $date;?>" disabled ><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<<!--footer-->
<footer>
    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h5><u>Media</u></h5>
                <p><a href="https://www.youtube.com" target="_blank" rel="noopener noreferrer" >YouTube</a></p>
                <p><a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer" >Instagram</a></p>
                <p><a href="https://www.tiktok.com" target="_blank" rel="noopener noreferrer" >TikTok</a></p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h5><u>Where To Find Suggested Products</u></h5>
                <p><a href="https://www.amazon.com" target="_blank" rel="noopener noreferrer" >Amazon</a></p>
                <p><a href="https://www.smartpakequine.com/" target="_blank" rel="noopener noreferrer" >SmartPak</a></p>
                <p><a href="https://www.doversaddlery.com/" target="_blank" rel="noopener noreferrer" >Dover Saddlery</a></p>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <h5><u>Contact Us</u></h5>
                <p><a href="mailto:someone@example.com" target="_blank" rel="noopener noreferrer" >Suggest Product</a></p>
            </div>
        </div>
    </div>
</footer>
</body>

</html>
