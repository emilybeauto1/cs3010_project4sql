<?php
$usernameErr = $passwordErr = $password1Err = $firstNameErr = $lastNameErr =
$phoneErr = $emailErr = $address1Err = $address2Err = $cityErr = $stateErr = $zipErr =
$genderErr = $martialErr = $dateErr = "";
$username = $password = $password1 = $firstName = $lastName = $phone = $email =
$address1 = $address2 = $city = $state = $zip = $gender = $martial = $date = "";
$isValid = false;


//make input usable and pretty
function clean_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    return htmlspecialchars($data);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //username check
    $isValid = true;
    if (!isset($_POST['username'])) {
        $usernameErr = "Username is required!";
        $isValid = false;
    } elseif (empty($_POST['username'])) {
        $usernameErr = "Username is required!";
        $isValid = false;
    } else {
        $username = clean_input($_POST['username']);
        if (strlen($username) < 6 || strlen($username) > 50) {
            $usernameErr = "Username must be between 6 and 50 characters long.";
            $isValid = false;
        }
    }

    //password check
    if (!isset($_POST['password'])) {
        $passwordErr = "Password is required!";
        $isValid = false;
    } elseif (empty($_POST['password'])) {
        $passwordErr = "Password is required!";
        $isValid = false;
    } else {
        $password = clean_input($_POST['password']);
        //check extra requirements
        if (!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,50}$/", $password)) {
            $passwordErr = "Must contain at least one capital, lowercase, number, and special character and have 8-50 characters";
            $isValid = false;
        }
    }

    //second password check and check for match
    if (!isset($_POST['password1'])) {
        $isValid = false;
    } elseif (empty($_POST['password1'])) {
        $password1Err = "Please enter a password that matches the first!";
        $isValid = false;
    } else {
        $password1 = clean_input($_POST['password1']);
        if ($password !== $password1) {
            $password1Err = "Passwords do not match!";
            $isValid = false;
        }
    }

    //check email
    if (!isset($_POST['email'])) {
        $isValid = false;
    } elseif (empty($_POST['email'])) {
        $emailErr = "Email is required!";
        $isValid = false;
    } else {
        $email = clean_input($_POST['email']);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //The filter_var() function was the safest and easiest way to check whether an email address was well-formed.
            $emailErr = "Invalid Format: Please enter correct email format!";
            $isValid = false;
        }
    }

    //check phone number
    if (!isset($_POST['phone'])) {
        $isValid = false;
    } elseif (empty($_POST['phone'])) {
        $phoneErr = "Phone Number needs to be filled out!";
        $isValid = false;
    } else {
        $phone = clean_input($_POST['phone']);
        if (!preg_match("/^(1-)?\d{3}-\d{3}-\d{4}$/", $phone)) {
            $phoneErr = "Please enter a valid phone number with the dashes!";
            $isValid = false;
        }
    }

    //check gender
    if (isset($_POST["gender"])) {
        $gender = clean_input($_POST["gender"]);
        if (empty($_POST["gender"])) {
            $genderErr = "Gender is required";
            $isValid = false;
        }
    } else {
        $genderErr = "Gender is required";
        $isValid = false;
    }

    //check martial status
    if (isset($_POST["status"])) {
        $martial = clean_input($_POST["status"]);
        if (empty($_POST["status"])) {
            $martialErr = "Martial Status is required";
            $isValid = false;
        }
    } else {
        $martialErr = "Martial Status is required";
        $isValid = false;
    }

    //check firstName
    if (!isset($_POST['firstName'])) {
        $isValid = false;
    } elseif (empty($_POST['firstName'])) {
        $firstNameErr = "First name is required!";
        $isValid = false;
    } else {
        $firstName = clean_input($_POST['firstName']);
        if (strlen($firstName) > 50) {
            $firstNameErr = "Please enter a first name less than 50 characters.";
            $isValid = false;
        }
    }

    //check lastName
    if (!isset($_POST['lastName'])) {
        $isValid = false;
    } elseif (empty($_POST['lastName'])) {
        $lastNameErr = "Last name is required!";
        $isValid = false;
    } else {
        $lastName = clean_input($_POST['lastName']);
        if (strlen($lastName) > 50) {
            $lastNameErr = "Please enter a last name less than 50 characters.";
            $isValid = false;
        }
    }

    //check for address 1
    if (!isset($_POST['address1'])) {
        $isValid = false;
    } elseif (empty($_POST['address1'])) {
        $address1Err = "Address is required!";
        $isValid = false;
    } else {
        $address1 = clean_input($_POST['address1']);
        if (strlen($address1) > 100) {
            $address1Err = "Please enter an Address between 1-100 characters.";
            $isValid = false;
        }
    }

    //check address 2
    if (isset($_POST['address2'])) {
        $address2 = clean_input($_POST['address2']);
    } else {
        if (strlen($address2) > 100) {
            $address2Err = "Please Enter Valid Address.";
            $isValid = false;
        }
    }

    //check city
    if (!isset($_POST['city'])) {
        $isValid = false;
    } elseif (empty($_POST['city'])) {
        $cityErr = "City is required!";
        $isValid = false;
    } else {
        $city = clean_input($_POST['city']);
        if (strlen($city) > 50) {
            $cityErr = "Please enter a city less than 50 characters.";
            $isValid = false;
        }
    }

    //check state
    if (isset($_POST['state'])) {
        $state = clean_input($_POST['state']);
        if (empty($_POST['state'])) {
            $stateErr = "Please select a state!";
            $isValid = false;
        }
    } else {
        $stateErr = "Please select a state!";
        $isValid = false;
    }

    //check zip
    if (!isset($_POST['zip'])) {
        $isValid = false;
    } elseif (empty($_POST['zip'])) {
        $zipErr = "Zip Code is required!";
        $isValid = false;
    } else {
        $zip = clean_input($_POST['zip']);
        if (!preg_match("/^[0-9]{5}(?:-[0-9]{4})?$/", $zip)) {
            $zipErr = "Please enter a zip code between 5-10 characters with hyphen.";
            $isValid = false;
        }
    }

    //check dob
    if (isset($_POST['dob'])) {
        $date = clean_input($_POST['dob']);
        if (empty($date)) {
            $dateErr = "Please select a date.";
            $isValid = false;
        }
    } else {
        $dateErr = "Please select a date.";
        $isValid = false;
    }
}

